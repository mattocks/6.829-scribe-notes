\documentclass{6046}

\usepackage{tikz}
\usetikzlibrary{arrows, calc,intersections,through,backgrounds}
\usepackage{amsmath}
\usepackage{algorithm}
\usepackage{algorithmic}

\author{Chiraag Juvekar}
\problemset{7}

\begin{document}
\section{Channel Coding - Shannon's Bound}
In 1948, Claude Shannon proposed a theoretical bound for the rate at which data
can be communicated over a noisy channel. This bound is called the channel
capacity, $C$, of the noisy channel. He further showed that if one were to
transmit over such a channel at a rate $R < C$, it was possible to achieve an
arbitrarily small error probability. In addition if one tried to transmit at any
rate $R > C$, one will always end up with some finite probability of error that
increases as the rate increases.

If the channel had a finite probability of corrupting the message, it may seem
that transmitter would have to endlessly repeat it's message to ensure that the
probability of errors goes to zero. Shannon's insight shows that it is indeed
possible to reliably communicate at a finite rate as long as it below the
channel capacity. Hence it is considered as a landmark result in information
theory.

\subsection{AWGN Channel}
Consider an tranmistter, \textbf{Tx}, and a receiver, \textbf{Rx},
communicating over an Additive White Gaussian Noise (AWGN) channel. \textbf{Tx} 
can transmit a message from a set of $M$ messages (for e.g. if this is set of
$m$-bit binary messages, then $M = 2^m$). To accomplish this \textbf{Tx}, 
transmits $n$ symbols over the real AWGN with noise power $N$. The signal 
strength at \textbf{Rx} is measured to be $S$.

Thus if $x_i$, $y_i$ and $w_i$ represent the transmitted symbols, received
symbols and noise respectively we have:
\begin{align*}
  y_i = x_i + w_i && \forall 1 \leq i \leq n 
\end{align*}

Thus if we represent the code-words in an $n$ dimensional space, we see that
$\bf{x}$ tend lie within a sphere of radius $\sqrt{n(S)}$. The received 
code-words, $\bf{y}$, tends to lie within a sphere of radius $\sqrt{n(S+N)}$. Finally
for large n received code-words lie on the surface of a n-dimensional sphere
centered at the transmitted code-words having radius $\sqrt{n(N)}$. For the
probability of error to be very small, the probability of the 2 spheres
intersecting must be very small. Thus the number of messages, $M$,  that we can
distinguish is equal to the number of smaller spheres we can pack within the
larger sphere. This is bounded as follows, 
\begin{align*}
  M &\leq \frac{\text{Volume}_{\text{Larger
  Sphere}}}{\text{Volume}_{\text{Smaller Sphere}}} \\
  &\leq \frac{(n(N+S))^{n/2}}{(n(N))^{n/2}} \\
  &\leq 2^{\frac{n}{2}\log_2\left(1+\frac{S}{N}\right)}
\end{align*}
If the set of messages is $m$-bit words we have, 
\begin{equation}
  R = \frac{m}{n} \leq \frac{1}{2}\log_2\left(1 + \frac{S}{N}\right) 
\end{equation}


% Add figure of spheres here
\begin{figure}[!h]
  \centering
  \begin{tikzpicture}
    \tikzset{space/.style={fill=black!2!white, draw=black!50!white}}
    \tikzset{codeword/.style={fill=blue!10!white, draw=blue!50!black, fill
    opacity=0.2}}
    % Main Sphere.
    \colorlet{codeword}{black} 
    \colorlet{codeword-space}{red!70!black}
    \colorlet{space}{orange}
    \coordinate (O) at ($ (0,0) $);
    \filldraw[space] (O) circle (3.5);
    \filldraw[red!10!white] (O) circle (3);
    \filldraw[codeword] ($ (-3.00, 0.00) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-2.00, 0.00) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-1.00, 0.00) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 0.00, 0.00) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 1.00, 0.00) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 2.00, 0.00) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 3.00, 0.00) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-2.50, 0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-1.50, 0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-0.50, 0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 0.50, 0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 1.50, 0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 2.50, 0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-2.00, 1.70) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-1.00, 1.70) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 0.00, 1.70) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 1.00, 1.70) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 2.00, 1.70) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-1.50, 2.55) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-0.50, 2.55) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 0.50, 2.55) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 1.50, 2.55) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-2.50,-0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-1.50,-0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-0.50,-0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 0.50,-0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 1.50,-0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 2.50,-0.85) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-2.00,-1.70) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-1.00,-1.70) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 0.00,-1.70) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 1.00,-1.70) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 2.00,-1.70) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-1.50,-2.55) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ (-0.50,-2.55) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 0.50,-2.55) + 0.15*(1.2*rand, rand)$) circle (0.5);
    \filldraw[codeword] ($ ( 1.50,-2.55) $) circle (0.5);
    \node[anchor=west] at (2.47, 2.47) (space) {$\sqrt{n(P+\sigma^2)}$};
    \draw[-stealth, thick] (0, 0) -- (2.47, 2.47);
    \node[anchor=east, red] at (-2.5, 2.1) (space) {$\sqrt{n(P)}$};
    \draw[-stealth, thick, red] (0, 0) -- (-2.1, 2.1);
    \node[anchor=west, blue!50!black] at (1.85, -2.90) (space) {$\sqrt{n(\sigma^2)}$};
    \draw[-stealth, thick, blue!50!black] (1.50, -2.55) -- (1.85, -2.90);
  \end{tikzpicture}
\caption{The code-words represented in an $n$-dimensional sphere}
\end{figure}

\subsection{Shannon's Random Codes}
Note that the above construction only establishes an upper bound on the
achievable rate and does not specify how we are to achieve this rate. Shannon 
suggested the use of the coding scheme using random codebooks to achieve this 
rate. In such a scheme the encoder chooses $M$ random points in the 
$n$-dimensional symbol space. Each message is then encoded to the corresponding 
points and transmitted as a series of $n$-symbols $\bf{x}$. It is assumed that 
the decoder has access to this codebook and on receiving the noisy version 
$\bf{y}$ chooses the candidate set $\mathcal{C}$ as follow, 
\begin{equation}
  \mathcal{C} = \{\hat{x}: ||\hat{x}-y||^2 \in (nN-\epsilon, nN+\epsilon)\}
\end{equation}
Decoding succeeds if $|\mathcal{C}| = 1$. if the set has 0 or multiple elements
then we say that decoding has failed. It can be shown that using this scheme we
can bound $P(\text{error}) < \epsilon$ as long as we use a rate $R$, 
\begin{equation}
  R = \frac{\log_2 M}{n} < \frac{1}{2}\log_2(1+\text{SNR}) - \delta_n(\epsilon)
\end{equation}

Unfortunately, for $\epsilon \to 0, \delta_n(\epsilon) \to 0$ we have $n \to
\infty$. Thus in order to use a rated code to get extremely low error rates we 
have to use larger and larger symbol lengths. The symbol lengths required for 
a given error probability are tabulated below.

\begin{table}[!h]
\centering
\begin{tabular}{c c}
  \hline
  $P(\text{error})$ & n \\
  \hline
  0.5 & $10^{3}$ \\
  0.1 & $10^{6}$ \\
  0.01 & $10^{10}$ \\
  \hline
\end{tabular}
\caption{Comparison of symbol length required to limit error probability}
\end{table}

\section{Limitations of Rated Codes}
As we have seen in the previous section in order for a code to be reliable it
has to be very long. Thus when using rated code the user is forced to choose
between long reliable codes and shorter unreliable codes. Rateless codes on the
other hand allows us to generate as many code-words as required on the fly. Thus
we can get away with transmitting extra code words only when an error takes
place. 

\subsection{Example of 2 Rated Codes}
Consider the following example with two rated codes A and B. We will calculate
the rate and latency observed when transmitting 1000 bits over this code.
\begin{table}
\centering
\begin{tabular}{l | c c}
  \hline
                      & Code A & Code B \\
  \hline
  Code Length ($n$)   & 500 symbols & 760 symbols \\
  $P(\text{success})$ & 0.7 & 0.95 \\
  Rate ($R$)          & $\frac{1000}{500}\times0.7 = 1.4$ & $\frac{1000}{760}\times0.95 = 1.25$ \\
  $95^{th}$ percentile latency & $>$1000  & 760 \\
  \hline 
\end{tabular}
\caption{An example of 2 rated showing the trade-off between bit-rate and
latency}
\end{table}

Note how the longer code has a lower latency but also suffers from a smaller
bit-rate.

\subsection{Bit-rate improvement with H-ARQ}
Now assume that code A is a prefix of code B. Thus when the first code word
cannot be decoded we only need to send the 260 additional symbols from code B.
In order to accomplish we will use the H-ARQ protocol which works as follows. We
first transmit some symbols and then wait for an ACK from the receiver. If we
receive and ACK then the transmission is complete and we move to the next
message. If not we transmit a few more symbols for the old message and then wait
for the ACK. This repeats until the receiver gets the correct message or until
the transmitter gives up.
\begin{table}[!h]
\centering
\begin{tabular}{l | c }
  \hline
                      & Code A is a prefix of Code B \\
  \hline
  Rate ($R$)          & $\frac{1000}{500+0.3\times260}\times0.95 = 1.4$  \\
  $95^{th}$ percentile latency & 760\\
  \hline 
\end{tabular}
\caption{Combined prefix code with H-ARQ}
\end{table}

Thus we see that the combined code has better latency than both the individual
code and still offers very good latency. Rateless codes exploit this very fact,
since they only transmit extra symbols for erroneous messages.

\section{Spinal Codes}
In this section we discuss some interesting properties of spinal codes and the
pruning decoder described in \cite{perry12}. The spinal code encoder is shown in
figure \ref{encoder}. The psuedo-random number generators allow us to generate
multiple code-words per k-bit message on the fly. The spine of seed values
creates a dependency between the code-words generated for the last messages and
the bits of the first message. This dependency ensures that the path cost for
the erroneous decoding is significantly higher than that for the correct path.

\begin{figure}[!h]
\centering
\includegraphics[width=0.5\textwidth]{img/encoder.png}
\caption{Spinal Code Encoder. Figure from \cite{perry12}}
\label{encoder}
\end{figure}

\subsection{Feasibility of Decoding}
Given a message $M$ made up of $m$ $k$-bit messages the decoding complexity of 3
decoders is considered. A full ML decoder that does not store any intermediate
result will require $\mathcal{O}(m2^mh)$ computation to complete the decoding.
A simple optimization is possible if we store all the results that were
generated at the previous steps. By traversing the tree we do not need to decode
the initial code-words again. This reduces the complexity to $\mathcal{O}(2^m)$.
Unfortunately this is still exponential in the size of the message. The final
optimization involves pruning the tree and only storing a fixed number of 
candidates. For a constant beam-width the decoder complexity is linear in both 
time and space. The bubble decoder that implements this is shown in figure
\ref{decoder}

\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{img/decoder.png}
\caption{Bubble Decoder. Figure from \cite{perry12}}
\label{decoder}
\end{figure}

\subsection{Puncturing}
If a code takes $l$ passes to be decoded successfully then the rate of the
spinal code is $k/l$. In order to get more flexibility while decoding and
allowing for a wider range of rates it is possible to puncture the spinal code.
Puncturing by a factor $\gamma$. Allows for all rates of the form $k\gamma/N$
where is a positive integer.

\subsection{Caching}
Caching the intermediate values of one pass so as to enable reuse in the next
pass is generally not very helpful. This is because falling out of the beam
generally happens very early, because there is not enough information to
correctly pick the right pruning. Thus very few values get reused and it is not
enough to justify the hardware cost.

\begin{thebibliography}{9}
    \bibitem{tse05}
      David Tse and Pramod Vishwanath, 
      Fundamentals of Wireless Communication, 
      Cambridge University Press, 2005

    \bibitem{perry12}
      Jonathan Perry and Peter Iannucci and Kermin Elliott Fleming and Hari
      Balakrishnan and Devavrat Shah, 
      Spinal Codes, 
      ACM SIGCOMM, 2012
\end{thebibliography}
\end{document}
